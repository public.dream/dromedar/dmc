# Distributed Mutable Containers (DMC)

This repository contains the specification of Distributed Mutable Containers (DMC) as well as a runnable Datalog implementation.

## Repository overview

- [`spec/`](spec/): Specification source (AsciiDoc and Turtle)
- [`public/`](public/): Specification artifacts that can be hosted on a web server
- [`examples/`](examples/): Datalog implementation

## License

[CC-BY-SA-4.0](./LICENSES/CC-BY-SA-4.0.txt) for specification.
[AGPL-3.0-or-later](./LICENSES/AGPL-3.0-or-later.txt) for code.
