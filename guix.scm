(use-modules
 (guix packages)
 ((guix licenses) #:prefix license:)
 (guix download)
 (guix git-download)
 (guix build-system gnu)
 (gnu packages)
 (gnu packages crypto)
 (gnu packages autotools)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages pkg-config)
 (gnu packages texinfo)

 ;; stuff for souffle
 (gnu packages compression)
 (gnu packages cpp)
 (gnu packages bison)
 (gnu packages flex)
 (gnu packages libffi)
 (gnu packages ncurses)
 (gnu packages pkg-config)
 (gnu packages sqlite))

(define-public mcpp
  (package
    (name "mcpp")
    (version "2.7.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://sourceforge/mcpp/mcpp/" version
                           "/mcpp-" version "-src.tar.gz"))
       (file-name (string-append "mcpp-" version ".tar.gz"))
       (sha256
        (base32
         "0r48rfghjm90pkdyr4khxg783g9v98rdx2n69xn8f6c5i0hl96rv"))))
    (build-system gnu-build-system)
    (home-page "http://mcpp.sourceforge.net/download.html")
    (synopsis "@code{mcpp} is an alternative C/C++ preprocessor")
    (description "@code{mcpp} is a C/C++ preprocessor with the highest
conformance which implements C90, C99 and C++98.  @code{mcpp} has plentiful
diagnostics and many @code{#pragmas}.  It is useful to check portability of your
program, and also useful to debug complicated macro.")
    (license license:bsd-2)))

(define-public souffle
  (package
    (name "souffle")
    (version "f25e65f784e723b4189556c826fe23b1b76b6098")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/souffle-lang/souffle/")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "15bjsjyrdha5rkn4hcdb863wdm60d2nzzah5vrl56bsc0rq77hm5"))))
    (build-system gnu-build-system)
    ;; TODO remove before submitting. Tests are extensive and take some time.
    (arguments '(#:tests? #f
                 #:phases (modify-phases %standard-phases
                            (add-after 'install 'wrap-binaries
                              (lambda* (#:key outputs #:allow-other-keys)
                                (let ((out (assoc-ref outputs "out")))
                                  (wrap-program (string-append out "/bin/souffle")
                                    `("PATH" ":" prefix (,(dirname (which "mcpp")))))
                                  #t))))))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("libtool" ,libtool)
       ("flex" ,flex)
       ("bison" ,bison)))
    (propagated-inputs
     `(("libffi" ,libffi)
       ("ncurses" ,ncurses)
       ("mcpp" ,mcpp)
       ("sqlite" ,sqlite)
       ("zlib" ,zlib)
       ("pkg-config" ,pkg-config)))
    (home-page "https://souffle-lang.github.io/")
    (synopsis "Soufflé is a logic programming language inspired by Datalog")
    (description " Soufflé is a logic programming language inspired by Datalog
that is frequently used as a domain-specific language for analysis
problems.  Soufflé can synthesize a native parallel C++ program from a logic
specification.")
    (license (license:fsf-free "https://oss.oracle.com/licenses/upl/"
                               "https://www.gnu.org/licenses/license-list.html#UPL"))))

(define-public guile-sodium
  (package
    (name "guile-sodium")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/openengiadina/guile-sodium")
             (commit "0344d63de6035f8e666085f88a49ffdcbaf0e7e5")))
       (file-name (git-file-name name version))
       (sha256 (base32 "00pr4j1biiklcr4q3x38fi45md866bql57aq2aima826jfkwws05"))))
    (build-system gnu-build-system)
    (arguments `())
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("pkg-config" ,pkg-config)
       ("texinfo" ,texinfo)))
    (inputs `(("guile" ,guile-3.0)))
    (propagated-inputs `(("libsodium" ,libsodium)))
    (synopsis "Guile bindings to libsodium.)")
    (description
     "This package provides bindings to libsodium which provides core cryptogrpahic primitives needed to build higher-level tools.")
    (home-page
     "https://gitlab.com/openengiadina/guile-sodium")
    (license license:gpl3+)))

(define-public guile-eris
  (package
    (name "guile-eris")
    (version "0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.com/openengiadina/eris")
               (commit "cd8fa22fa99bb23ce68a8a2689c4634ead550c5d")))
        (file-name "guile-eris-0.1-checkout")
        (sha256 (base32 "06bj5sqr08a9sv10a3iqah1waj62jwmx0g4x5f6kvz5m29vlrwwn"))))
    (build-system gnu-build-system)
    (arguments '())
    (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
    (inputs `(("guile" ,guile-3.0)))
    (propagated-inputs
     `(("guile-sodium" ,guile-sodium)))
    (synopsis
      "Guile implementation of Encoding for Robust Immutable Storage (ERIS)")
    (description
      "guile-eris is the reference implementation of Encoding for Robust Immutable Storage (ERIS). ERIS allows arbirtary content to be encoded into uniformly sized, encrypted blocks that can be reassembled using a short read-capability.")
    (home-page
      "https://gitlab.com/openengiadina/eris")
    (license license:gpl3+)))

(define-public guile-schemantic
  (package
    (name "guile-schemantic")
    (version "0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.com/openengiadina/guile-schemantic")
               (commit "286626137e5f58e9a06ca7c2e5b4aab9cf75f3a2")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1n3ziqw97711i4g7cwq89rvbxw18l6wdz6dl19db62q0mahfw2s0"))))
    (build-system gnu-build-system)
    (arguments `())
    (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
    (inputs `(("guile" ,guile-3.0)))
    (propagated-inputs `(("guile-rdf" ,guile-rdf)))
    (synopsis "Guile library for the Semantic Web")
    (description
      "Guile Schemantic is a Guile library for the Semantic Web and implements the Resource Description Framework (RDF).")
    (home-page
      "https://gitlab.com/openengiadina/guile-schemantic")
    (license license:gpl3+)))

(define-public guile-rdf-signify
  (package
    (name "guile-rdf-signify")
    (version "0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.com/openengiadina/rdf-signify")
               (commit "174cafde3a4ded1ca512bf06e8dd4b12842824ae")))
        (file-name (git-file-name name version))
        (sha256 (base32 "0v9sl4mv2d1cpv3kd59n7jnn9xfkiqv1q2pk60j4rfjaabvyijx7"))))
    (build-system gnu-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
                      (add-after 'unpack 'run-hall
                                 (lambda _
                                   (setenv "HOME" "/tmp")   ; for ~/.hall
                                   (invoke "hall" "dist" "-x"))))))
    (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)
        ("guile-hall" ,guile-hall)))
    (inputs `(("guile" ,guile-3.0)))
    (propagated-inputs
      `(("guile-schemantic" ,guile-schemantic)
        ("guile-sodium" ,guile-sodium)))
    (synopsis
      "A RDF vocabulary for cryptographic signatures using Ed25519.")
    (description "")
    (home-page
      "https://gitlab.com/openengiadina/rdf-signify")
    (license license:gpl3+)))


(package
  (name "guile-dcm")
  (version "0.1")
  (source "./guile-dcm-0.1.tar.gz")
  (build-system gnu-build-system)
  (arguments
   '(#:phases
     (modify-phases %standard-phases
       (add-after 'unpack 'run-hall
         (lambda _
           (setenv "HOME" "/tmp")   ; for ~/.hall
           (invoke "hall" "dist" "-x"))))))
  (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)
      ("texinfo" ,texinfo)
      ("guile-hall" ,guile-hall)))
  (inputs `(("guile" ,guile-3.0)))
  (propagated-inputs
   `(("guile-sodium" ,guile-sodium)
     ("guile-eris" ,guile-eris)
     ("guile-schemantic" ,guile-schemantic)
     ("guile-rdf-signify" ,guile-rdf-signify)
     ("souffle" ,souffle)))
  (synopsis "Distributed Mutable Containers")
  (description "")
  (home-page
    "https://gitlab.com/openengiadina/dcm")
  (license license:gpl3+))
