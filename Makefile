.PHONY: spec
spec: public/index.html

public/index.html: spec/dmc.adoc spec/dmc.ttl
	asciidoctor -o $@ -a webfonts! $<
	cp spec/dmc.ttl public/dmc.ttl

.PHONY: publish
publish: spec
	rsync -rav -e ssh public/ qfwfq.inqlab.net:/srv/http/inqlab.net-projects/dmc/ --delete
